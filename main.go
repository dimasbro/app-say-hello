package main

import (
	"fmt"

	go_say_hello "gitlab.com/dimasbro/go-say-hello/v2"
)

func main() {
	fmt.Println(go_say_hello.SayHello("Eko"))
}
